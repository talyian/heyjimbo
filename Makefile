build: build/js/server.js
	@echo "done"

node_modules:
	npm i

build/js/server.js: build/es6/server.js node_modules
	node_modules/.bin/babel -d build/js --preset babel-presets-es2015 build/es6

build/es6/server.js: src/server.ts src/content_db.ts
	node_modules/.bin/tsc

typings: node_modules
	node_modules/.bin/typings

reqs:
	npm install

run:
	npm start

start:
	npm start
